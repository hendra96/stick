<?php
session_start();
?>
<html lang="en">
	<head>
		<title>Aplikasi Perhitungan Pajak Bumi dan Bangunan</title>
		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet"> 

		<!-- Custom styles for this template -->
			<link href="css/custom.css" rel="stylesheet">
			<link href="css/flexslider.css" rel="stylesheet">
			<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<script type="text/javascript" src="js/custom.js"></script>
	</head>	
		<body background="img/aa.png">
			<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container">
				  <div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					 <img src="914858_09242508012015_dirjen_pajak.jpg" width="88" height="68"><font color="#000000">DIREKTORAT JENDERAL PAJAK</font></div>
					<div class="collapse navbar-collapse appiNav">
						<ul class="nav navbar-nav">
							<li><a href="index.html">Home</a></li>
							<li><a href="Registrasi.php">Sign Up</a></li>
							<li><a href="Login.php">Sign In</a></li>
							<li><a href="Kontak.php">Contact Us</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</div>
				<center>
					
					<br>
					<br>
					<br>					
						<h2><b><font color="#000000"> Aplikasi Perhitungan Pajak Bumi dan Bangunan</font></b></h2>					
						<center>
							<p><b><font size="+3">
							<?php
							echo "Selamat Datang <strong>".$_SESSION['Username']."</strong>";
							?>
							<br/>
							</font></b></p>
						</center>		       	
					<div id="featureWrap">
						<div class="container">
							<div class="row">
								<div class="col-sm-4 text-center feature">
									<i class="fa fa-one icon">1</i>
									<h3>Masukkan Identitas Wajib Pajak</h3>
									<p>
									<font color="#000000">Anda harus mengisi form identitas wajib pajak secara lengkap dan benar
									</font>
									</p>
								</div>
								<div class="col-sm-4 text-center feature">
									<i class="fa fa-two icon">2</i>
									<h3>Masukkan Data Objek Pajak</h3>
									<p>
									<font color="#000000">Masukkan data objek pajak yang dimiliki</font></p>
								</div>
								<div class="col-sm-4 text-center feature">
									<i class="fa fa-three icon">3</i>
									<h3>Hitung PBB Terutang</h3>
									<p> <font color="#000000">klik buttom hitung maka akan muncul besarnya PBB yang harus dibayar</font></p>
								</div>
							</div>
						</div>
					</div> <!-- /featureWrap -->
				<footer>
					<div class="container">
						<div class="row">
							<div class="col-xs-12 text-center">
								<p>Copyright &copy; 2016 Luziana Dewi</p>					
							</div>
						</div>
					</div>
				</footer>
				<script src="js/flexslider.js"></script>	
		</body>
</html>